#include <iostream>
#include <vector>
#include <map>

#include "room.cpp"
#include "player.cpp"

int main()
{

    struct Colors {
        enum roomTextColors{
            ORANGE, YELLOW, GREEN, RED, PURPLE, BLUE
        };
    };

    const std::string ANSI_COLOR[] = {
            "\033[0;33m", // ORANGE
            "\033[1;33m", // YELLOW
            "\033[0;32m", // GREEN
            "\033[0;31m", // RED
            "\033[0;35m", // PURPLE
            "\033[0;34m"  // BLUE
    };
    const std::string ANSI_COLOR_RESET = "\033[0m";

    // Create rooms
    Room startRoom("You are in a dimly lit room.", ANSI_COLOR[Colors::ORANGE]);
    Room hallway("You are in a long hallway.", ANSI_COLOR[Colors::YELLOW]);
    Room treasureRoom("You have entered a treasure room!", ANSI_COLOR[Colors::GREEN]);
    Room poisonRoom("You enter into a room with a venemous trap! You lose 25 HP.", ANSI_COLOR[Colors::RED]);
    Room undeadDwarfenKingsStronghold("You have fallen into the Dwarven King's keep! You lose 30 HP - FIGHT FOR YOUR LIFE!", ANSI_COLOR[Colors::PURPLE]);
    Room evacuatedShelter("Inside you find a room which was once used to shelter orphans of the war, a massacre must have occurred as the floor is still stained red...", ANSI_COLOR[Colors::BLUE]);
    Room magesStudy("You find a lab, with many glowing flasks. The green one catches your eye.", ANSI_COLOR[Colors::GREEN]);

    // Define exits between rooms
    startRoom.addExit("north", &hallway);
    startRoom.addExit("south", &poisonRoom);
    startRoom.addExit("west", &evacuatedShelter);
    poisonRoom.addExit("north", &startRoom);
    hallway.addExit("south", &startRoom);
    hallway.addExit("north", &treasureRoom);
    treasureRoom.addExit("south", &hallway);
    treasureRoom.addExit("east", &undeadDwarfenKingsStronghold);
    treasureRoom.addExit("west", &magesStudy);
    undeadDwarfenKingsStronghold.addExit("west", &treasureRoom);
    evacuatedShelter.addExit("east", &startRoom);
    evacuatedShelter.addExit("north", &magesStudy);
    magesStudy.addExit("south", &evacuatedShelter);
    magesStudy.addExit("east", &treasureRoom);

    // Create items
    Item key("Key", "A shiny key that looks important.", 0);
    Item sword("Sword", "A sharp sword with a golden hilt.", 100);
    Item fallenStar("Fallen Star", "A piece of a star that must have crashed onto the lands long ago...", 100);
    Item kingsPendant("King's Pendant", "A pendant once belonging to a dwarven king during the cave wars... it overflows with mana.", 5000);
    Item magesNotebook("Mage's notebook", "A notebook kept by a mage before he fell into the abyss...", 50);

    // Add items to rooms
    startRoom.addItem(key);
    treasureRoom.addItem(sword);
    magesStudy.addItem(magesNotebook);

    // Create a player
    Player player("Alex The Slayer", 100, 0);
    player.setLocation(&startRoom);

    // Create NPC
    //NPC shopkeeper("Darren");
    //shopkeeper.setLocation(&evacuatedShelter);

    // Game loop
    while (player.getHealth() > 0)
    {

        std::cout << std::endl
                  << "Current Location: " << player.getLocation()->getDescription() << std::endl;
        std::cout << "Player Name: " << player.getName() << std::endl;
        std::cout << "Current HP: " << player.getHealth() << std::endl;
        std::cout << "Gold: " << player.getGold() << std::endl;
        std::cout << "Items in the room: " << std::endl;
        for (Item &item : player.getLocation()->getItems())
        {
            std::cout << "- " << item.getName() << ": " << item.getDescription() << std::endl;
        }

        std::cout << "**************************" << std::endl;
        std::cout << "1. Look around" << std::endl;;
        std::cout << "**************************" << std::endl;;
        std::cout << "2. Interact with an item" << std::endl;;
        std::cout << "**************************" << std::endl;;
        std::cout << "3. Move to another room" << std::endl;;
        std::cout << "**************************" << std::endl;;
        std::cout << "4. Use item in inventory" << std::endl;;
        std::cout << "**************************" << std::endl;;
        std::cout << "5. Quit" << std::endl;
        std::cout << "**************************" << std::endl;;

        int choice;
        std::cin >> choice;

        if (choice == 1)
        {
            std::cout << "You look around the room." << std::endl;
        }
        else if (choice == 2)
        {
            std::cout << "Enter the name of the item you want to interact with: ";
            std::string itemName;
            std::cin >> itemName;

            for (Item &item : player.getLocation()->getItems())
            {
                if (item.getName() == itemName)
                {
                    item.interact();
                    player.addToInventory(item);			// add the item to inventory...
                    player.getLocation()->removeItem(item); // ...but remove it from the room
                    break;
                }
            }
        }
        else if (choice == 3)
        {
            std::cout << "Enter the direction (e.g. north, south): ";
            std::string direction;
            std::cin >> direction;

            Room *nextRoom = player.getLocation()->getExit(direction);
            if (nextRoom != nullptr)
            {
                player.setLocation(nextRoom);
                if (nextRoom == &evacuatedShelter){
                    //shopkeeper.InteractWithNPC(player, shopkeeper);
                }

            }
            if (nextRoom != nullptr)
            {
                player.setLocation(nextRoom);
                if (nextRoom == &undeadDwarfenKingsStronghold)
                    player.takeDamage(35);
                std::cout << "You move to the next room." << std::endl;
            }
            else
            {
                std::cout << "You can't go that way." << std::endl;
            }
        }
        else if (choice == 4)
        {
            std::cout << "Your inventory: " << std::endl;
            for (int i = 0; i < player.getInventory().size(); i++)
            {
                std::cout << " -" << player.getInventory()[i].getName() << std::endl; // print all items in inventory
            }

            std::cout << "Enter the name of the item from your inventory you want to use: ";
            std::string itemName;
            std::cin >> itemName;
            for (Item &item : player.getInventory())
            {
                if (item.getName() == itemName)
                {
                    std::cout << "You used '" << item.getName() << "'" << std::endl;
                    if (item.getName() == "Key" && player.getLocation() == &treasureRoom)
                    {
                        std::cout << "You use the key to open a treasure chest...inside you find a small piece of a fallen star. It must be valuable." << std::endl;
                        player.addToInventory(fallenStar);
                        player.removeFromInventory(key);
                    }
                    else if (item.getName() == "fallenStar")
                    {
                        std::cout << "You heal yourself for 15 HP" << std::endl;
                        player.takeDamage(-15); // adds HP (-- = +)
                        player.removeFromInventory(fallenStar);
                    }
                    else if (item.getName() == "Sword" && player.getLocation() == &undeadDwarfenKingsStronghold)
                    {
                        std::cout << "You slash the fallen king in two, dropping his pendant in the process!" << std::endl;
                        player.addToInventory(kingsPendant);
                        std::cout << "You win!" << std::endl;
                        break;
                    }
                    else
                    {
                        std::cout << "The item had no effect." << std::endl;
                    }
                }
            }
        }
        else if (choice == 5)
        {
            // Quit the game
            std::cout << "Goodbye!" << std::endl;
            exit(0); // instead of break so that you don't get the "you died" message on quit
        }
        else
        {
            std::cout << "Invalid choice. Try again." << std::endl;
        }
    }

    std::cout << "You died." << std::endl;

    return 0;
}
