#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "item.cpp"
#include "room.cpp"

class Character
{
private:
    std::string name;
    int health;
    std::vector<Item> inventory;
    int gold;
    Room *location;

public:
    Character(const std::string &playerName, int playerHealth, int totalGold)
    {
        name = playerName;
        health = playerHealth;
        gold = totalGold;
    }

    void takeDamage(int damage)
    {
        health -= damage; // newHealth = currentHealth - damageTaken
    }

    void AddGold(int amount) {
        gold += amount;
        std::cout << "You gained " << amount << " gold. Total gold: " << gold << std::endl;
    }

    std::vector<Item> getInventory()
    {
        return inventory;
    }

    void addToInventory(Item item)
    {
        inventory.push_back(item); // adds item to inventory
    }

    void removeFromInventory(Item item)
    {
        for (int i = 0; i < inventory.size(); i++) // loop through all items in player's inventory
        {
            if (inventory[i].getName() == item.getName())
            {
                inventory.erase(inventory.begin()+i); // if found, remove it
            }
        }
    }

    std::string getName()
    {
        return name;
    }

    int getHealth()
    {
        return health;
    }

    int getGold(){
        return gold;
    }

    void SellItem(const Item& item) {
        int itemValue = item.getValue();
        AddGold(itemValue);
    }

    void setLocation(Room *room)
    {
        location = room;
    }
};


class Player : public Character
{
private:
    Room *location;

public:
    Player(const std::string &playerName, int playerHealth, int totalGold) : Character(playerName, playerHealth, totalGold)
    {
        // inheriting constructor function from Character
    }

    Room* getLocation()
    {
        return location;
    }
};
