#pragma once

#include <string>
#include <iostream>

class Item
{
private:
    std::string name;
    std::string description;
    int value;

public:
    Item(const std::string &itemName, const std::string &desc, const int &val)
    {
        name = itemName;
        description = desc;
        value = val;
    }

    void interact()
    {
        std::cout << "You added '" << name << "' to your inventory." << " value: " << value << std::endl;
    }

    std::string getName()
    {
        return name;
    }

    std::string getDescription()
    {
        return description;
    }

    int getValue() const
    {
        return value;
    }
};
