#pragma once

#include <iostream>
#include <string>
#include <map>
#include <vector>

#include "item.cpp"

class Room
{
private:
    std::string description;
    std::string color; // Variable to add color to room text
    std::map<std::string, Room *> exits;
    std::vector<Item> items;

public:
    Room(std::string desc, std::string col) : description(desc), color(col) {} // Modify constructor to accept color parameter

    std::string getDescription() const {
        return description;
    }

    void addItem(const Item &item)
    {
        items.push_back(item); // adds item to vector
    }

    const void removeItem(Item &item)
    {
        for (int i = 0; i < items.size(); i++) // loop through all items in current room
        {
            if (items[i].getName() == item.getName())
            {
                items.erase(items.begin()+i); // if found, remove it
            }
        }
    }

    void addExit(const std::string& description, Room *room)
    {
        exits.insert(std::make_pair(description, room)); // {k, s} form of inserting a pair
    }

    std::string getDescription()
    {
        return description;
    }

    std::vector<Item> getItems()
    {
        return items;
    }

    Room *getExit(std::string dir)
    {
        return exits[dir]; // 'index' is referred to by its key (description in this case)
    }
};

